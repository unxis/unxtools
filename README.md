# unxtools

A collection of useful scripts for various simple tasks bit too small to
warrant a separate project.

# Windows Subsystem for Linux #

The [wsl](wsl) directory contains scripts that are useful when using the
[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/faq).

## ssh-agent ##

Windows Subsystem for Linux does not run a full-fledged virtual Linux system,
rather it provides a compatability layer.  Thus some of the normal session and
startup found on a Linux system does not work in the same manner.

These scripts allow you to roughly emulate the normal use of ssh-agent on a
Linux desktop system by sharing the keys between multiple interactive terminals
via a shared configuration file.  A single ssh-agent session will be started
and shared with subsequent instances of bash, when the final instance exits
gracefully, the ssh-agent will be terminated.

### Usage ###

Include the script found in [``start-ssh-agent.sh``](wsl/ssh-agent/start-ssh-agent.sh)
into your ``$HOME/.bashrc``.

Inclue the script found in [``stop-ssh-agent.sh``](wsl/ssh-agent/stop-ssh-agent.sh)
in your ``$HOME/.bash_logout``.

# Licensing #

All software in this project is licensed under the [MIT License](LICENSE)
unless otherwise indicated in the software source code or documentation.

## Bee icon license

The [Bee PNG Icon](https://www.pngrepo.com/svg/48159/bee) is used under a
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.
