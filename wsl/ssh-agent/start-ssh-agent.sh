# start ssh-agent for WSL
SSHAGENTINFO=/tmp/${USER}-ssh-agent/conf
if [ -f $SSHAGENTINFO ]; then
  printf "Reading ssh-agent socket info from %s.\n" $SSHAGENTINFO
  source $SSHAGENTINFO
else
  (umask 0077 ; mkdir -p `dirname $SSHAGENTINFO`; ssh-agent > $SSHAGENTINFO)
  source $SSHAGENTINFO
fi

