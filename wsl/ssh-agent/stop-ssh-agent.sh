# shut down our ssh-agent when we close the final shell
SHELLPIDS=$( pgrep bash )
SHELLCOUNT=$( echo $SHELLPIDS | wc -w )
if [[ $SHELLCOUNT -eq 1 ]]; then
  ssh-agent -k && rm -f /tmp/${USER}-ssh-agent/conf
fi

